import React, { useState } from 'react';
import { Paper, Typography, Grid, Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import PropTypes from 'prop-types';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
    width:"100%"
  },
  courtColumn: {
    minWidth: '60px',
  },
  courtText: {
    minHeight: '50px',
    textAlign: 'center',
    background: '#ddd',
    margin: '1px',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  timeRow: {
    minWidth: '60px',
    textAlign: 'center',
  },
  bookingCell: {
    border: '1px solid #ddd',
    minHeight: '50px',
    position: 'relative',
    cursor: 'pointer',
  },
  scrollableContent: {
    overflowX: 'auto',
  },
  navigationButtons: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(2),
  },
}));

const BookingCustomCalendar = ({ courts, times }) => {
  const classes = useStyles();
  const [currentDate, setCurrentDate] = useState(moment());

  const handlePrevDay = () => {
    setCurrentDate(currentDate.clone().subtract(1, 'day'));
  };

  const handleNextDay = () => {
    setCurrentDate(currentDate.clone().add(1, 'day'));
  };

  return (
    <div className={classes.root}>
      {/* Date navigation buttons */}
      <div className={classes.navigationButtons}>
        <Button variant="outlined" onClick={handlePrevDay}>
          Previous Day
        </Button>
        <Typography variant="h6">{currentDate.format('MMMM D, YYYY')}</Typography>
        <Button variant="outlined" onClick={handleNextDay}>
          Next Day
        </Button>
      </div>

      <Grid container spacing={1}>
        {/* Top side - Time rows */}
        <Grid item xs={9} className={classes.scrollableContent}>
          <div className={`${classes.timeRow} ${classes.scrollableContent}`}>
            <Grid container>
              <Grid item xs={3} />

              {times.map((time) => (
                <Grid key={time} item xs={1}>
                  <Typography>{time}</Typography>
                </Grid>
              ))}
            </Grid>
          </div>

          {/* Booking cells */}
          {courts.map((court) => (
            <Grid key={court.id} container>
              <Grid item xs={3}>
                <div className={`${classes.courtColumn} ${classes.scrollableContent}`}>
                  <Typography className={classes.courtText}>{court.name}</Typography>
                </div>
              </Grid>

              {times.map((time) => (
                <Grid key={`${court.id}-${time}`} item xs={1} className={classes.bookingCell}>
                  {/* Render booking data here */}
                </Grid>
              ))}
            </Grid>
          ))}
        </Grid>
      </Grid>
    </div>
  );
};

BookingCustomCalendar.propTypes = {
  courts: PropTypes.array.isRequired,
  times: PropTypes.array.isRequired,
};

export default BookingCustomCalendar;
